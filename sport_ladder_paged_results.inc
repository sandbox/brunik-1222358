<?php

// $Id$

/**
 * @file
 * defines the 'sport_ladder_paged_results' menu callback for the sport_ladder module
 *
 * defines sport_ladder_paged_results, the rountines it calls and any ancilliary hooks
 * so that a user can view and possibly edit a resultString having reached the paged results  
 */

/**
 * Page callback.
 */
function sport_ladder_paged_results()//shows the results to players and non-players, giving appropiate functionality 
{
	if(db_query("SELECT count(cid) from {sport_ladder_results}")->fetchField())//are there any records in the ladder ? 
	{
		global $user;//currently logged in user
		$timenow = time();
		$expirydays = variable_get('sport_ladder_challenge_expiry', 7);
		$daysecs = 60 * 60 * 24;
		$expiry_period = $expirydays * $daysecs;

		$query = db_select('sport_ladder_results', 'r')->extend('PagerDefault')->limit(10);
		$query->fields('r', array('cid','challenger_games', 'challengee_games', 'result_time', 'commentary', 'effect_on_ladder', 'challenger_old_rank', 'win_action' ));
		$result_all = $query->execute();

		foreach($result_all as $row)//loop round all the results retrieved from the db table
		{
			$resChall = db_query("SELECT * FROM {sport_ladder_challenges} WHERE $row->cid = cid");
			$ChallengeRow = $resChall->fetchObject();
			$nameChallenger = db_query("SELECT name FROM {users} WHERE $ChallengeRow->challenger = uid")->fetchField();
			$nameChallengee = db_query("SELECT name FROM {users} WHERE $ChallengeRow->challengee = uid")->fetchField();
			$resultDesc = ($row->challenger_games > $row->challengee_games) ? "and won" : (($row->challenger_games < $row->challengee_games) ? "but lost" : "however drew");
			$resultString = sprintf("%s challenged %s %s %u-%u", $nameChallenger, $nameChallengee, $resultDesc,  $row->challenger_games, $row->challengee_games);
			$nowtimesecs = time();//used by both routines below
			$WithinExpiryPeriod = ($ChallengeRow->challenge_time + $expiry_period) > $nowtimesecs;
			$NumChallSinceOurResult = db_query("SELECT count(cid) FROM {sport_ladder_challenges} WHERE challenge_time > $row->result_time")->fetchField();
			$NumResultsSinceOurResult = db_query("SELECT count(cid) FROM {sport_ladder_results} WHERE result_time > $row->result_time")->fetchField();
			$WasUserTheChallenger = ($user->uid == $ChallengeRow->challenger) ? 1 : 0;//user can be just browsing results (and not party to result)
			$WasUserTheChallengee = ($user->uid == $ChallengeRow->challengee) ? 1 : 0;//user can be just browsing results (and not party to result)
			$date = date('D dS M Y G:H:i',$row->result_time);//format date for results table
			#debug($row, 'sport_ladder_paged_results');
			#$constrStr = sprintf("WithinExpiryPeriod %u, WasUserTheChallenger %u, WasUserTheChallengee %u, NumChallSinceOurResult %u, NumResultsSinceOurResult %u", $WithinExpiryPeriod, $WasUserTheChallenger, $WasUserTheChallengee, $NumChallSinceOurResult, $NumResultsSinceOurResult);
			#debug($constrStr, 'sport_ladder_paged_results');
			if(($WithinExpiryPeriod && ($WasUserTheChallenger || $WasUserTheChallengee))&& !$NumChallSinceOurResult && !$NumResultsSinceOurResult)
			{
				$OpponentUid = $WasUserTheChallenger ? $ChallengeRow->challengee : $ChallengeRow->challenger;
				$OppUserName = db_query("SELECT name FROM {users} WHERE $OpponentUid = uid")->fetchField();
				$days_left_of_chall = (int)((($ChallengeRow->challenge_time + ($daysecs * $expirydays)) - $nowtimesecs)/$daysecs);
				$data[] = array($row->cid, $date, l($resultString, "sport_ladder_result/$OpponentUid/$WasUserTheChallenger/$row->cid/$row->challenger_games/$row->challengee_games/$row->challenger_old_rank/$row->commentary", array('attributes' => array('title' => t("Edit the result of your game versus %name, you've only got %days days left to fulfill the fixture", array('%name' => $OppUserName, '%days' => $days_left_of_chall,)), 'style' => t('color:orange')))), $row->commentary, $row->effect_on_ladder );
			}
			else
			{
				$data[] = array($row->cid, $date, $resultString, $row->commentary, $row->effect_on_ladder);
			}
		}
		$header = array('cid', 'Time Result Submitted', 'Result', 'Match Report', 'Effect on Ladder' );
		$output = theme('table', array('header' => $header, 'rows' => $data));
		return $output .= theme('pager');
	}
	else
	{
		return t('There are no results to show in the ladder!');
	}
}
