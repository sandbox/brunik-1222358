<?php

// $Id$

/**
 * @file
 * defines the callback for the menu 'ladder'
 *
 * Displays a sport ladder of current rankings from which
 * authenticated users can challenge others and record results
 */

/**
 * Page callback.
 */
function sport_ladder_fn()//shows the ladder to players and non-players, giving appropiate functionality 
{
	if(db_query("SELECT count(uid) from {sport_ladder}")->fetchField())//are there any records in the ladder ? 
	{
		$items = array();//return value to be populated by links or text if no players
		global $user;//currently logged in user
		$user_in_cooloff = CoolOffAfterEnteringFavourableResult($user->uid);
		#debug("user in cool-off $user_in_cooloff", 'sport_ladder_fn');
		$timenow = time();
		$expirydays = variable_get('sport_ladder_challenge_expiry', 7);
		$daysecs = 60 * 60 * 24;
		$ladder_user_result = db_query("SELECT * FROM {sport_ladder} WHERE $user->uid = uid");
		$ladder_user = $ladder_user_result->fetchObject();
		$challsteps = variable_get('sport_ladder_steps', 2) + 1;//need to add 1 to steps as config returns zero indexed array values
		$result_all = db_query("SELECT * FROM {sport_ladder} ORDER BY rank ASC");
		foreach($result_all as $row)//loop round all the players in the ladder retrieved from the db table
		{
			$userl = user_load($row->uid);//get the user object for this row from the users table
			$display_string = sprintf("\"%s\" - RANK[%u] user[%u] challenges[%u] current cid[%u]", $userl->name, $row->rank, $row->uid, $row->number_challenges, $row->matched);
			if($user->uid != 1 && user_is_logged_in())
			{
				//user is logged in and there are players in the ladder
				if($ladder_user->rank < $row->rank)//has the row got a worse rank than me?
				{
					//not interested in challenging, but may want to enter result of challenge
					if($row->opp_uid == $user->uid)//has the lower ranked player challenged you?
					{
						$days_left_of_chall = (int)((($row->challenge_time + ($daysecs * $expirydays)) - $timenow)/$daysecs);
						$items[] = l($display_string, "sport_ladder_result/$userl->uid/0/$ladder_user->matched", array('attributes' => array('title' => t("Enter the result of your game versus %name, you've only got %days days left to fulfill the fixture", array('%name' => $userl->name, '%days' => $days_left_of_chall,)), 'style' => t('color:orange'))) );
					}
					else//hasn't challenged you
					{
						if($ladder_user->matched != NULL)//have I got a match?
						{
							if($row->opp_uid)//has the row got a match?
							{
								$days_left_of_chall = (int)((($row->challenge_time + ($daysecs * $expirydays)) - $timenow)/$daysecs);
								$items[] = l($display_string, "user/$row->uid", array('attributes' => array('title' => t("%name currently matched with %oppname, and they have %days days left to play the fixture and maybe challenge you soon if they won, follow link for profile", array('%name' => $userl->name, '%oppname' => $row->oppname, '%days' => $days_left_of_chall,)), 'style' => t('color:pink'))) );
							}
							else//the row hasn't got a match
							{
								$items[] = l($display_string, "user/$row->uid", array('attributes' => array('title' => t("Hi I'm %name, I'm hoping to play you soon, especially if you lost your match,  follow link to see my profile", array('%name' => $userl->name)), 'style' => t('color:blue'))) );
							}
						}
						else//no I haven't got a match
						{
							if($row->opp_uid)//has the row got a match?
							{
								$days_left_of_chall = (int)((($row->challenge_time + ($daysecs * $expirydays)) - $timenow)/$daysecs);
								$items[] = l($display_string, "user/$row->uid", array('attributes' => array('title' => t("%name is currently matched with %oppname, and they have %days days left to play the fixture, follow link for profile", array('%name' => $userl->name, '%oppname' => $row->oppname, '%days' => $days_left_of_chall,)), 'style' => t('color:pink'))) );
							}
							else//the row hasn't got a match
							{
								$items[] = l($display_string, "user/$row->uid", array('attributes' => array('title' => t("Too lowly for you, just see %name's profile", array('%name' => $userl->name)), 'style' => t('color:blue'))) );
							}
						}
					}
				}
				else if($ladder_user->rank > $row->rank)//has the row got a better rank than me ?
				{
					//interested in challenging, if in range and not matched
					if($ladder_user->number_challenges == 0)//if I'm new I can challenge anyone above me
					{
						if($row->matched != NULL)//is the row matched?
						{
							//can't challenge if the player is matched, and can't be me as I'm new and have not made any challenges
							$days_left_of_chall = (int)((($row->challenge_time + ($daysecs * $expirydays)) - $timenow)/$daysecs);
							$items[] = l($display_string, "user/$row->uid", array('attributes' => array('title' => t("Can't challenge %name as already matched against %oppname, and they have %days days left to play the fixture, follow link to see %name's profile", array('%name' => $userl->name, '%oppname' => $row->oppname, '%name' => $userl->name, '%days' => $days_left_of_chall,)), 'style' => t('color:red'))) );
						}
						else//not matched, can challenge
						{
							if($user_in_cooloff == false)
							{
							$items[] = l($display_string, "sport_ladder_challenge/$row->uid/$userl->name/$userl->mail/0/$row->number_challenges", array('attributes' => array('title' => t("Make a challenge on %name by clicking this link", array('%name' => $userl->name)), 'style' => t('color:green'))) );
							}
							else
							{
								$items[] = l($display_string, "user/$row->uid", array('attributes' => array('title' => t("Sorry you could have challenged %name, but you have just entered a favourable result and you have to wait a day until you can challenge, so follow link to see %name's profile", array('%name' => $userl->name, )), 'style' => t('color:red'))) );
							}
						}
					}
					else if($row->rank + $challsteps < $ladder_user->rank)//is the row within my challenge range?
					{
						//out of challenge range
						if($row->opp_uid == $user->uid)//is this who I challenged?
						{
							//yes I did challenge this row
							$days_left_of_chall = (int)((($row->challenge_time + ($daysecs * $expirydays)) - $timenow)/$daysecs);
							$items[] = l($display_string, "sport_ladder_result/$userl->uid/1/$ladder_user->matched", array('attributes' => array('title' => t("Hey this is who I challenged, %name, and we've got %days days to play our game, follow link to enter result", array('%name' => $userl->name, '%days' => $days_left_of_chall,)), 'style' => t('color:orange'))) );
						}
						else//no it's not who I challenged
						{
							//but they may have a game, or I may have a game
							if($ladder_user->matched)
							{
								if($row->matched)
								{
									//they have a game and I have a game
									$days_left_of_chall = (int)((($row->challenge_time + ($daysecs * $expirydays)) - $timenow)/$daysecs);
									$items[] = l($display_string, "user/$row->uid", array('attributes' => array('title' => t("%name has got a match with %oppname, and they have %days days left to play the fixture, follow link to see profile", array('%name' => $userl->name, '%oppname' => $row->oppname, '%days' => $days_left_of_chall,)), 'style' => t('color:purple'))) );

								}
								else//row is not matched
								{
									//only I have a game
									$items[] = l($display_string, "user/$row->uid", array('attributes' => array('title' => t("You may be able to play %name IF you win your game, just see their profile", array('%name' => $userl->name)), 'style' => t('color:blue'))) );
								}
							}
							else//too lofty for you
							{
								if($row->matched)
								{
									//I don't have a game but they do have a game
									$days_left_of_chall = (int)((($row->challenge_time + ($daysecs * $expirydays)) - $timenow)/$daysecs);
									$items[] = l($display_string, "user/$row->uid", array('attributes' => array('title' => t("%name has got a match with %oppname, so if they lose you COULD play them soon, and they have %days days left to play the fixture, follow link to see profile", array('%name' => $userl->name, '%oppname' => $row->oppname, '%days' => $days_left_of_chall,)), 'style' => t('color:purple'))) );
								}
								else//the row does not have a match
								{
									//neither of us have a game, just too lofty
									$items[] = l($display_string, "user/$row->uid", array('attributes' => array('title' => t("Sorry can't challenge as %name is too lofty for you, just see their profile", array('%name' => $userl->name)), 'style' => t('color:blue'))) );
								}
							}
						}
					}
					else//potentially challengeable
					{
						if($row->opp_uid == $user->uid)//is this who I challenged?
						{
							//yes I did challenge this row
							$days_left_of_chall = (int)((($row->challenge_time + ($daysecs * $expirydays)) - $timenow)/$daysecs);
							$items[] = l($display_string, "sport_ladder_result/$userl->uid/1/$ladder_user->matched", array('attributes' => array('title' => t("Hey this is who I challenged - %name, and we've got %days days to play our game, follow link to enter result", array('%name' => $userl->name, '%days' => $days_left_of_chall,)), 'style' => t('color:orange'))) );
						}
						else if($row->matched)//no I didn't challenge this row, but are they matched
						{
							//yes they are matched
							if($ladder_user->matched == NULL)//am I matched?
							{
								//no I'm not matched, disappointing that the row is matched
								$days_left_of_chall = (int)((($row->challenge_time + ($daysecs * $expirydays)) - $timenow)/$daysecs);
								$items[] = l($display_string, "user/$row->uid", array('attributes' => array('title' => t("Sorry %name is already matched with %oppname, and they have %days days left to play the fixture, so follow link to see %name's profile", array('%name' => $userl->name, '%oppname' => $row->oppname, '%name' => $userl->name, '%days' => $days_left_of_chall,)), 'style' => t('color:red'))) );
							}
							else//yes I am matched
							{
								$days_left_of_chall = (int)((($row->challenge_time + ($daysecs * $expirydays)) - $timenow)/$daysecs);
								$items[] = l($display_string, "user/$row->uid", array('attributes' => array('title' => t("%name has got a match with %oppname, and they have %days days left to play the fixture, follow link to see profile", array('%name' => $userl->name, '%oppname' => $row->oppname, '%days' => $days_left_of_chall,)), 'style' => t('color:purple'))) );
							}
						}
						else//not matched, can challenge, if I'm not matched
						{
							if($ladder_user->matched != NULL)//am I matched?
							{
								//yes I am matched
								$items[] = l($display_string, "user/$row->uid", array('attributes' => array('title' => t("You may be able to play me, %name, soon if you won your match", array('%name' => $userl->name)), 'style' => t('color:blue'))) );
							}
							else//I'm not matched, so I can go ahead and challenge this row
							{
								if($user_in_cooloff == false)
								{
									$items[] = l($display_string, "sport_ladder_challenge/$row->uid/$userl->name/$userl->mail/$ladder_user->number_challenges/$row->number_challenges", array('attributes' => array('title' => t("Go ahead and challenge %name by following this challenge link, and good luck!", array('%name' => $userl->name)), 'style' => t('color:green'))) );
								}
								else
								{
									$items[] = l($display_string, "user/$row->uid", array('attributes' => array('title' => t("Sorry you could have challenged %name, but you have just entered a favourable result and you have to wait a day until you can challenge, so follow link to see %name's profile", array('%name' => $userl->name, )), 'style' => t('color:red'))) );
								}
							}
						}
					}
				}
				else//it's me !
				{
					if($row->matched)//am I matched ?
					{
						$days_left_of_chall = (int)((($row->challenge_time + ($daysecs * $expirydays)) - $timenow)/$daysecs);
						$items[] = l($display_string, "user/$row->uid", array('attributes' => array('title' => t("It's me %name!, did I win? if so enter result on my opponent %name_opp's orange link -- We've only got %days days to play our game", array('%name' => $userl->name, '%name_opp' => $row->oppname, '%days' => $days_left_of_chall,)), 'style' => t('color:brown'))) );
					}
					else//not matched
					{
						$items[] = l($display_string, "user/$row->uid", array('attributes' => array('title' => t("It's me \"%name\", Come on, make a challenge! ", array('%name' => $userl->name)), 'style' => t('color:black'))) );
					}
				}
			}
			else//just display the ladder without the ability to play
			{
				$items[] = $display_string;
			}
		}
		return array('#markup' => theme('item_list', array('items' => $items)));	
	}
	else
	{
		return t('There are no players in the ladder!');
	}
}

function CoolOffAfterEnteringFavourableResult($uid)//return > 0 if you have entered a favourable result within the last day
{
	$EpochTimeSingleDayAgo = time() - (24 * 60 * 60);
	return  db_query("SELECT count(cid) FROM {sport_ladder_results} WHERE ($uid = submitter) AND (result_in_favour_of_submitter = 1) AND (result_time > $EpochTimeSingleDayAgo)")->fetchField();
}

