<?php

// $Id$

/**
 * @file
 * defines the 'sport_ladder_challenge' menu callback for the sport_ladder module
 *
 * defines sport_ladder_challenge_fn, the rountines it calls and any ancilliary hooks
 * so that a user can make a challenge having followed ladder link
 */

/**
 * Page callback of the challenge form
 */
function sport_ladder_challenge_fn() //this needs to be here but it doesn't do anything !!!
{
	//empty because the alter routine seems to be the way to get at the passed args
}

function sport_ladder_form_sport_ladder_challenge_fn_alter(&$form, &$form_state, $form_id) {

	list($opp_uid, $opp_name, $opp_mail, $myNumChalls, $oppNumChalls) = $form_state['build_info']['args'];//get the args from the url into vars
	$teststring = sprintf("%s", $opp_name);//build a string for the challenge form

	$form['msg2opp'] = array(
		'#title' => t('Optional Message to Send to your Opponent'),
		'#type' => 'textarea',
		'#description' => t('Your Message'),
	);

	$form['challenge'] = array(
		'#title' => t('Your Opponent'),
		'#type' => 'fieldset',
		'#description' => l($teststring, "user/$opp_uid",array('attributes' => array('title' => t("It's me \"%name\", Come on, make a challenge! ", array('%name' => $opp_name)), 'style' => t('color:purple'))) )
	);

	$form['challenge']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Submit your challenge')
	);
}

/**
 * Form submission handler for sport_ladder_challenge_fn
 */
function sport_ladder_challenge_fn_submit($form, &$form_state)//do something with the values submitted by the challenge form 
{
	//args passed to 'sport_ladder_challenge' are e.g /$row->uid/$userl->name/$userl->mail/0/$row->number_challenges

	global $user, $language;
	$form_state['build_info']['args'][] = $form_state['values']['msg2opp'];//append the optional msg to challengee to array of form url args

	//first write a new rec to the challenges table
	$rec1 = new stdClass();
	$rec1->challenger = $user->uid;
	$rec1->challengee = $form_state['build_info']['args'][0];
	$rec1->challenge_time = time();
	$ret = drupal_write_record('sport_ladder_challenges', $rec1);//the $rec1 is passed as ref and cid field should update
	$challstr = sprintf("ret %s challeng: er %u ee %u time %d cid %u",$ret, $rec1->challenger, $rec1->challengee,  $rec1->challenge_time, $rec1->cid); 
	$form_state['build_info']['args'][] = $rec1->cid;//append the cid as the 5th arg
	$tmpcid = $rec1->cid;
	$tmpchalltime = $rec1->challenge_time;

	//second update the ladder with the challenger state
	$rec2 = new stdClass();
	$rec2->uid = $user->uid;
	$rec2->number_challenges = $form_state['build_info']['args'][3] + 1;
	$rec2->matched = $tmpcid;
	$rec2->opp_uid = $form_state['build_info']['args'][0];
	$rec2->oppname = $form_state['build_info']['args'][1];
	$rec2->challenge_time = $tmpchalltime;
	$challladderwritestr = sprintf("before chall-ladd-write : challenger %u  num-chall %u matched %u",$rec2->uid, $rec2->number_challenges, $rec2->matched); 
	$ret = drupal_write_record('sport_ladder', $rec2, 'uid');//update because pass PK 3rd arg
	$challladderwritestr = sprintf("ret %s chall-ladd-write : challenger %u num-chall %u matched %u",$ret, $rec2->uid, $rec2->number_challenges, $rec2->matched); 
	$form_state['build_info']['args'][3] = $rec2->number_challenges;//updated now the challenge has been submitted(gets passed to email handler)

	//third update the ladder with the challenger state
	$rec3 = new stdClass();
	$rec3->uid = $form_state['build_info']['args'][0];
	$rec3->number_challenges = $form_state['build_info']['args'][4];
	$rec3->matched = $tmpcid;
	$rec3->opp_uid = $user->uid;
	$rec3->oppname = $user->name;
	$rec3->challenge_time = $tmpchalltime;
	$challladderwritestr = sprintf("before chall-ladd-write : challengee %u num-chall %u matched %u",$rec3->uid, $rec3->number_challenges, $rec3->matched); 
	$ret = drupal_write_record('sport_ladder', $rec3, 'uid');//update because pass PK 3rd arg
	$challladderwritestr = sprintf("ret %s chall-ladd-write : challengee %u num-chall %u matched %u",$ret, $rec3->uid, $rec3->number_challenges, $rec3->matched); 

	//send the email to the opponent
	$message = drupal_mail('sport_ladder', 'challenge', $form_state['build_info']['args'][2], language_default(), $form_state['build_info']['args'], $user->name); 
	$teststr = sprintf("email send %s, notifying the challenge", ($message['result'] == 1 ? "good":"bad"));
	#debug($teststr, 'sport_ladder_challenge_fn_submit');

	$oppmail = $form_state['build_info']['args'][2];
	drupal_set_message("Your challenge and optional messsage has been sent to your opponent \"$rec2->oppname\" at \"$oppmail\"");
	$form_state['redirect'] = 'ladder';//redirect to the ladder after submitting the challenge
}

/**
 * Implements hook_mail().
 */
function sport_ladder_mail($key, &$message, $params)//callback that sends the email 
{
	switch ($key) 
	{
	case 'challenge':
		global $user;
		list($opp_uid, $opp_name, $opp_mail, $myNumChalls, $oppNumChalls, $text, $cid) = $params;
		$parsstr = sprintf("%s has challenged you. If you want to arrange the match on email please reply to \"%s\"\nHis message to you is \"%s\"\nThe challenge identification number(cid) of this match is %u",  strtoupper($user->name), $user->mail,$text, $cid);
		#debug($parsstr);
		$message['subject'] = "You've been challenged by $user->name";
		$message['body'][] = $parsstr;
		break;
	case 'challenge_expiry'://this key never got reached probably because the calling module was cron, so use PHP mail there instead
		$message['subject'] = "the challenge made on you has expired";
		$message['body'][] = "you've been demoted, please see the ladder and results tables";
		break;
	}
}


