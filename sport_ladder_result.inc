<?php

// $Id$

/**
 * @file
 * defines the 'sport_ladder_result' menu callback for the sport_ladder module
 *
 * defines sport_ladder_result_fn, the rountines it calls and any ancilliary hooks
 * so that a user can enter a result having followed ladder link
 */


/**
 * Page callback of the result form
 */
function sport_ladder_result_fn() //this needs to be here but it doesn't do anything(see note in body)
{
	//empty because the alter routine seems to be the way to get at the passed args
}


function sport_ladder_form_sport_ladder_result_fn_alter(&$form, &$form_state, $form_id)//defines the result form 
{
	$numargs = sizeof($form_state['build_info']['args']);
	$challenger_games = 0;
	$challengee_games = 0;
	$report = '';
	if($numargs > 3)
	{
		$challenger_games = $form_state['build_info']['args'][1] ? $form_state['build_info']['args'][3]: $form_state['build_info']['args'][4];
		$challengee_games = $form_state['build_info']['args'][1] ? $form_state['build_info']['args'][4]: $form_state['build_info']['args'][3];
		if($numargs == 7)
		{
			$report = $form_state['build_info']['args'][6];
		}
		#debug("num args passed $numargs, er $challenger_games, ee $challengee_games, report $report", 'sport_ladder_form_sport_ladder_result_fn_alter');
	}
	$oppUserLoad = user_load($form_state['build_info']['args'][0]);//user id of opponent of user

	$form['commentary'] = array(
		'#title' => t('Optional commentary on the match'),
		'#type' => 'textarea',
		'#description' => t('Add you report on the game which can be seen on the results page'),
		'#default_value' => $report,
		'#resizable' => FALSE,
		'#cols' => 60,//this is the default
		'#rows' => 5,//this is the default
	);

	$form['my_games'] = array(
		'#title' => t('Games won by you'),
		'#type' => 'select',
		'#description' => t('Please select how many games you won'),
		'#options' => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9),
		'#default_value' => $challenger_games,
		'#required' => true,//this is irrelevant because the default value fills it in
	);

	$form['op_games'] = array(
		#'#title' => t('How many games she won'),
		'#title' => "Games won by $oppUserLoad->name",
		'#type' => 'select',
		#'#description' => t('Please select how many games she won'),
		'#description' => "Please select how many games $oppUserLoad->name won",
		'#options' => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9),
		'#default_value' => $challengee_games,
		'#required' => true,//this is irrelevant because the default value fills it in

	);


	$form['challenge']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Submit your result')
	);
}

/**
 * Form validation handler for sport_ladder_result_fn
 */
function sport_ladder_result_fn_validate($form, &$form_state) 
{
	//no validation required now that we use drop down list of numbers 0-9,
	//and never worked out clean way of promting the question whether the
	//user intended to actually enter a draw, but could be work in progress
}

/**
 * Form submission handler for sport_ladder_result_fn
 */
function sport_ladder_result_fn_submit($form, &$form_state)//does stuff with the values from the result submission form 
{
	global $user, $language;

	$winaction = variable_get('sport_ladder_winner_action', 0);//0 is swap, 1 is shuffle
	$shuffle_or_swap_str = $winaction ? 'shuffle' : 'swap';

	$oppuid = $form_state['build_info']['args'][0];//user id of opponent of user
	$IamTheChallenger = $form_state['build_info']['args'][1];//1 indicates user was challenger, 0 for challengee
	$cid = $form_state['build_info']['args'][2];//challenge id of the match

	$res = db_query("SELECT * FROM {sport_ladder} WHERE $user->uid = uid");
	$myrow = $res->fetchObject();

	$res = db_query("SELECT * FROM {sport_ladder} WHERE $oppuid = uid");
	$oprow = $res->fetchObject();
	$oppUserLoad = user_load($oppuid);//can't use the oppname from ladder in case of edit result that field will have been reset first posting of result

	$result_rec = new stdClass();
	$result_rec->cid = $cid;
	$result_rec->commentary = $form_state['values']['commentary'];
	$result_rec->result_time = time();
	$result_rec->win_action = $winaction;
	$result_rec->effect_on_ladder = 'no change in ladder positions';//default string, could be changed
	$result_rec->submitter = $user->uid;

	//complication is that both parties of a challenge can enter the result, so need to take account from whose perspective the result is being submitted
	if($IamTheChallenger)
	{
		$result_rec->challenger_games = $form_state['values']['my_games'];
		$result_rec->challengee_games = $form_state['values']['op_games'];

		$numargs = sizeof($form_state['build_info']['args']);
		if($numargs == 3)//caller is from ladder page
		{
			$result_rec->challenger_old_rank = $myrow->rank;//store the challenger's ranking when she challenged
			if($form_state['values']['my_games'] > $form_state['values']['op_games'])//does the ranking need changing?
			{
				//yes it will need new rankings, as it's a win for the challenger
				$myrow->rank = $oprow->rank;//user(challenger) wins and takes opp rank
				if($myrow->number_challenges == 1 || $winaction == 1)//shuffle if configured or it's a first time challenge
				{
					++$oprow->rank;
					shuffle_down_inbetween_ranks($myrow->rank, $result_rec->challenger_old_rank);
				}
				else//configured to swap
				{
					$oprow->rank = $result_rec->challenger_old_rank;//opp just takes my old rank in case of swap
				}
				$result_rec->effect_on_ladder = "$user->name moved up to rank $myrow->rank and $oppUserLoad->name moved down to $oprow->rank after $shuffle_or_swap_str";
				$result_rec->result_in_favour_of_submitter = 1;
			}
		}
		else if($numargs > 3)//caller is from paged results
		{
			$chall_old_rank = $form_state['build_info']['args'][5];
			if(($form_state['build_info']['args'][3] > $form_state['build_info']['args'][4]))
			{
				//it was a win for the challenger
				if(!($form_state['values']['my_games'] > $form_state['values']['op_games']))//does the ranking need changing?
				{
					//reversal found - now found the challenger did not win
					$tmpChallengerWinningRank = $myrow->rank;
					$myrow->rank = $chall_old_rank;//back to old position
					if($myrow->number_challenges == 1 || $winaction == 1)//shuffle if configured or it's a first time challenge
					{
						shuffle_up_inbetween_ranks($chall_old_rank, $oprow->rank);
						--$oprow->rank;//do this AFTER shuffle
					}
					else//configured to swap
					{
						$oprow->rank = $tmpChallengerWinningRank;//opp just takes my old winning result rank in case of swap
					}
					$result_rec->effect_on_ladder = "$user->name moved down to rank $myrow->rank and $oppUserLoad->name moved up to $oprow->rank after winaction $shuffle_or_swap_str";
					$result_rec->result_in_favour_of_submitter = 0;
				}
				else
				{
					$result_rec->result_in_favour_of_submitter = 1;
				}
			}
			else if(!($form_state['build_info']['args'][3] > $form_state['build_info']['args'][4]))
			{
				//it was not a win for the challenger
				if(($form_state['values']['my_games'] > $form_state['values']['op_games']))//does the ranking need changing?
				{
					//now found the challenger did win
					$myrow->rank = $oprow->rank;//this will always be the case
					if($myrow->number_challenges == 1 || $winaction == 1)//shuffle if configured or it's a first time challenge
					{
						++$oprow->rank;
						shuffle_down_inbetween_ranks($myrow->rank, $chall_old_rank);
					}
					else//configured to swap
					{
						$oprow->rank = $chall_old_rank;//opp just takes my old rank in case of swap
					}
					$result_rec->effect_on_ladder = "$user->name moved up to rank $myrow->rank and $oppUserLoad->name moved down to $oprow->rank after winaction $shuffle_or_swap_str";
					$result_rec->result_in_favour_of_submitter = 1;
				}
				else
				{
					$result_rec->result_in_favour_of_submitter = 0;
				}
			}
		}
	}
	else//I am the challengee
	{
		$result_rec->challenger_games = $form_state['values']['op_games'];
		$result_rec->challengee_games = $form_state['values']['my_games'];
		$numargs = sizeof($form_state['build_info']['args']);
		if($numargs == 3)//caller is from ladder page
		{
			$result_rec->challenger_old_rank = $oprow->rank;//store op rank in case of swap
			if($form_state['values']['my_games'] < $form_state['values']['op_games'])//does the ranking need changing?
			{
				//yes it will need new rankings
				$oprow->rank = $myrow->rank;//this will always be the case
				if($oprow->number_challenges == 1 || $winaction == 1)//shuffle if configured or it's a first time challenge
				{
					++$myrow->rank;
					shuffle_down_inbetween_ranks($oprow->rank, $result_rec->challenger_old_rank);
				}
				else//configured to swap
				{
					$myrow->rank = $result_rec->challenger_old_rank;//I just take opp old rank in case of swap
				}
				$result_rec->effect_on_ladder = "$user->name moved down to rank $myrow->rank and $oppUserLoad->name moved up to $oprow->rank after $shuffle_or_swap_str";
			}
			else
			{
				$result_rec->result_in_favour_of_submitter = 1;
			}
		}
		else//caller is from paged results
		{
			$chall_old_rank = $form_state['build_info']['args'][5];
			if(($form_state['build_info']['args'][3] > $form_state['build_info']['args'][4]))
			{
				//it was a win for the challenger
				if(!($form_state['values']['op_games'] > $form_state['values']['my_games']))//does the ranking need changing?
				{
					//reversal found - now found the challenger did not win
					$tmpChallengerWinningRank = $oprow->rank;
					$oprow->rank = $chall_old_rank;//back to old position
					if($oprow->number_challenges == 1 || $winaction == 1)//shuffle if configured or it's a first time challenge
					{
						shuffle_up_inbetween_ranks($chall_old_rank, $myrow->rank);
						--$myrow->rank;//do this AFTER shuffle
					}
					else//configured to swap
					{
						$myrow->rank = $tmpChallengerWinningRank;//opp just takes my old winning result rank in case of swap
					}
					$result_rec->effect_on_ladder = "$user->name moved up to rank $myrow->rank and $oppUserLoad->name moved down to $oprow->rank after winaction $shuffle_or_swap_str";
					$result_rec->result_in_favour_of_submitter = 1;
				}
				else
				{
					$result_rec->result_in_favour_of_submitter = 0;
				}
			}
			else if(!($form_state['build_info']['args'][3] > $form_state['build_info']['args'][4]))
			{
				//it was not a win for the challenger
				if(($form_state['values']['op_games'] > $form_state['values']['my_games']))//does the ranking need changing?
				{
					//now found the challenger did win
					$oprow->rank = $myrow->rank;//this will always be the case
					if($myrow->number_challenges == 1 || $winaction == 1)//shuffle if configured or it's a first time challenge
					{
						++$myrow->rank;
						shuffle_down_inbetween_ranks($oprow->rank, $chall_old_rank);
					}
					else//configured to swap
					{
						$myrow->rank = $chall_old_rank;//opp just takes my old rank in case of swap
					}
					$result_rec->effect_on_ladder = "$user->name moved down to rank $myrow->rank and $oppUserLoad->name moved op to $oprow->rank after winaction $shuffle_or_swap_str";
					$result_rec->result_in_favour_of_submitter = 0;
				}
				else
				{
					$result_rec->result_in_favour_of_submitter = 1;
				}
			}

		}
	}
	reset_ladder_row_after_match($myrow);//makes NULL all ladder db fields except rank and uid
	reset_ladder_row_after_match($oprow);//makes NULL all ladder db fields except rank and uid

	if(($ret = drupal_write_record('sport_ladder', $myrow, 'uid')) == SAVED_UPDATED)//update ladder with my record after the result
	{
		watchdog('sport_ladder_result_fn_submit', "sport_ladder updated: submitter details: uid $myrow->uid, rank $myrow->rank, numchalls $myrow->number_challenges" );
	}
	if(($ret = drupal_write_record('sport_ladder', $oprow, 'uid')) ==  SAVED_UPDATED)//update ladder with my record after the result
	{
		watchdog('sport_ladder_result_fn_submit', "sport_ladder updated: opponent details: uid $oprow->uid, rank $oprow->rank, numchalls $oprow->number_challenges" );
	}

	if($numargs == 3)
	{
		$ret = drupal_write_record('sport_ladder_results', $result_rec);//write new result to the results table 
		$oldRank = $result_rec->challenger_old_rank;
	}
	else//the result form has been submitted after a link from the paged results tables
	{
		$ret = drupal_write_record('sport_ladder_results', $result_rec, 'cid');//update the results table with new corrected details of the match
		$oldRank = $chall_old_rank;
	}

	$newOrupdResWrite = $ret == 1 ? 'new' : 'upd';
	$resStr = "$result_rec->submitter/$user->name submitted $newOrupdResWrite rec written to 'sport_ladder_results': cid $result_rec->cid, er games $result_rec->challenger_games, ee games $result_rec->challengee_games, time $result_rec->result_time, old-rank $oldRank, $result_rec->effect_on_ladder";
	#debug($resStr, 'sport_ladder_result_fn_submit');
	watchdog('sport_ladder_result_fn_submit', $resStr);

	$mygames = $form_state['values']['my_games'];//save for the form set msg
	$opgames = $form_state['values']['op_games'];//save for the form set msg
	drupal_set_message("Your '$mygames - $opgames' result against '$oppUserLoad->name' has been recorded and the ladder updated");//posts a msg atop the subsequent ladder page with result submission summary

	$form_state['redirect'] = 'ladder';//redirects user to ladder rankings page after submitting result
}


function shuffle_down_inbetween_ranks($from, $dest) 
{
	$result_inbetweens = db_query("SELECT * FROM {sport_ladder} WHERE (rank > $from) AND (rank < $dest) ORDER BY rank ASC");
	foreach($result_inbetweens as $row)
	{
		++$row->rank;
		if(($ret = drupal_write_record('sport_ladder', $row, 'uid')) == SAVED_UPDATED)//update ladder with shuffled record 
		{
			watchdog('sport_ladder_result_fn_submit', "sport_ladder updated: shuffled details: uid $row->uid, rank $row->rank, numchalls $row->number_challenges" );
		}
	}
}

function shuffle_up_inbetween_ranks($from, $dest) 
{
	$result_inbetweens = db_query("SELECT * FROM {sport_ladder} WHERE (rank <= $from) AND (rank > $dest) ORDER BY rank ASC");
	foreach($result_inbetweens as $row)
	{
		--$row->rank;
		if(($ret = drupal_write_record('sport_ladder', $row, 'uid')) == SAVED_UPDATED)//update ladder with shuffled record 
		{
			watchdog('sport_ladder_result_fn_submit', "sport_ladder updated: shuffled details: uid $row->uid, rank $row->rank, numchalls $row->number_challenges" );
		}
	}
}

function reset_ladder_row_after_match(&$row) 
{
	$row->matched = NULL;
	$row->opp_uid = NULL;
	$row->oppname = NULL;
	$row->challenge_time = NULL;
}
