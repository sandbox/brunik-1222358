<?php
// $Id$

/**
 * @file
 * Administration page callbacks for the sport_ladder module.
 */

/**
 * Form builder. Configure sport_ladder.
 *
 * @ingroup forms
 * @see system_settings_form().
 */

function sport_ladder_admin_settings() {

  $form['sport_ladder_steps'] = array( 
      '#type' => 'select', 
      '#title' => t('Users may challenge this many places above them in the sport_ladder'), 
      '#default_value' => variable_get('sport_ladder_steps', 2),//zero based index, 2 => 3 steps 
      '#description' => t('Decide how many steps'), 
      '#options' => array(1, 2, 3, 4, 5, 6, 7, 8, 9),//'sport_ladder_steps' value of 2 will show 3 on the drop down
      ); 
  
  $form['sport_ladder_link_name'] = array( 
      '#type' => 'textfield', 
      '#title' => t('Ladder link'), 
      '#default_value' => variable_get('sport_ladder_link_name', 'Ladder'), 
      '#description' => t('Decide the Name for the Menu Ladder Link, e.g Chess Ladder, Squash Ladder'), 
      ); 


  $form['sport_ladder_challenge_expiry'] = array(
      '#type' => 'textfield',
      '#title' => t('Challenge Expiry'),
      '#description' => t('Enter the challenge expiry, which is the number of days after which, should a result not be entered, the challenger wins by default'),
      '#default_value' => variable_get('sport_ladder_challenge_expiry', 7)
      );

  $form['sport_ladder_winner_action'] = array(
      '#type' => 'radios',
      '#title' => t('Action upon successful challenge'),
      '#description' => t('Select a method for rearranging ladder after successful challenge'),
      '#options' => array(
        t('Players Swap Positions'),
        t('Successful Challenger Inserted Above Challengee')
        ),
      '#default_value' => variable_get('sport_ladder_winner_action', 0) // Default to 'Players Swap Positions'
      );

  return system_settings_form($form, TRUE); 
}

/**
 * sport_ladder_admin_settings_validate
 *
 */
function sport_ladder_admin_settings_validate($form, $form_state) {

  $expiry = $form_state['values']['sport_ladder_challenge_expiry'];
  if ((!is_numeric($expiry)) || ($expiry < 0))//must be a number and not negative, 0 allowed because useful for testing of aggressive cron challenge expiry routine
  {
    form_set_error('sport_ladder_challenge_expiry', t('Please enter a non negative number'));
  }
} 
